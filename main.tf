provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "DemoInstance" {
    ami = "ami-0cb8d7d7bff978ba4"
    instance_type = "t2.micro"
    count = 1
    
    tags = {
        Name = "Demo"
        }
}
